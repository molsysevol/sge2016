# Created on 22/02/2016 by gvbarroso
# Last modified 28/08/2017
# Look at various correlations between F* and other variables.

################################################################################

# Packages needed:
library(plyr)
library(reshape2)
library(ggplot2)
library(cowplot)
library(Hmisc)
dir.create("Figures")

################################################################################

sge.genes <- read.table("Results/SGE_GeneData.txt", stringsAsFactors = FALSE)
sge.paths <- read.table("Results/SGE_PathwayData.txt", stringsAsFactors = FALSE)

################################################################################
#
# NOISE PLOTS
#
################################################################################

dat <- sge.genes[, c("Mean", "Variance", "FanoFactor", "Noise", "Fstar")]
dat <- rename(dat, replace = c("FanoFactor" = "Fano factor", "Fstar" = "F*"))
dat <- melt(data = dat,
            id.vars = "Mean", variable.name = "SGEtype", value.name = "SGEvalue")

p1a <- ggplot(dat, aes(x = Mean, y=SGEvalue))
p1a <- p1a + geom_point(shape = ".", color = "grey50") + geom_smooth(method = "lm", color = "black", se = FALSE)
p1a <- p1a + facet_wrap(~SGEtype, scales = "free_y", ncol = 2)
p1a <- p1a + scale_x_log10() + scale_y_log10()
p1a <- p1a + theme_bw() + ylab("SGE measure")
p1a

# Alternative representation with discretization:
dat <- sge.genes[, c("Mean", "Variance", "FanoFactor", "Noise", "Fstar")]
dat <- rename(dat, replace = c("FanoFactor" = "Fano factor", "Fstar" = "F*"))
dat <- melt(data = dat,
            id.vars = "Mean", variable.name = "SGEtype", value.name = "SGEvalue")
dat$MeanClass <- cut2(dat$Mean, m = 3000, levels.mean = TRUE)

dat2 <- ddply(.data = dat, .variables = c("MeanClass", "SGEtype"),
                 .fun = plyr::summarize,
              median = median(SGEvalue),
              lo95 = quantile(SGEvalue, prob=0.025),
              hi95 = quantile(SGEvalue, prob=0.975))

p1a2 <- ggplot(dat2, aes(x = as.numeric(as.character(MeanClass)), y=median))
p1a2 <- p1a2 + geom_quantile(data=dat, aes(x=Mean, y=SGEvalue), col = "grey50")
p1a2 <- p1a2 + geom_point() + geom_errorbar(aes(ymin = lo95, ymax = hi95))
p1a2 <- p1a2 + facet_wrap(~SGEtype, scales = "free_y", ncol = 2)
p1a2 <- p1a2 + scale_x_log10() + scale_y_log10()
p1a2 <- p1a2 + theme_bw() + ylab("SGE measure") + xlab("Mean expression")
p1a2


# Distribution of F*
p1b <- ggplot(sge.genes, aes(Fstar))
p1b <- p1b + geom_histogram(aes(y=..density..), bins = 30)
p1b <- p1b + geom_density(alpha=.5, fill="grey30", size = 1)
p1b <- p1b + scale_x_log10() + geom_vline(xintercept = 1, size = 1, linetype = "dotted")
p1b <- p1b + theme_bw() + xlab("F*")
p1b


# Combine them:
pp <- plot_grid(p1a2, p1b, labels = "AUTO")
pp

ggsave(pp, filename = "Figures/MeasuresOfStochasticity.pdf", width = 12, height = 6)

################################################################################
#
# COMPARISON WITH DATASET 1
#
################################################################################

sge.genes.1 <- read.table("../Dataset1/Results/SGE_GeneData.txt", header = TRUE, stringsAsFactors = FALSE)
sge.genes.1$Fstar1 <- sge.genes.1$Fstar
sge.genes.1$Mean1 <- sge.genes.1$Mean
sge.genes.1 <- sge.genes.1[, c("Ensembl_id", "Mean1", "Fstar1")]
sge.genes.comp <- merge(sge.genes, sge.genes.1, by = "Ensembl_id")
dim(na.omit(sge.genes.comp[,c("Fstar", "Fstar1")]))

require(Hmisc)
require(plyr)
sge.genes.comp$Fstar1.class <- cut2(sge.genes.comp$Fstar1, g = 15, levels.mean = TRUE)
dat.fstar <- ddply(.data = sge.genes.comp, .variables = "Fstar1.class", .fun = plyr::summarize,
                  Fstar1.num = as.numeric(as.character(unique(Fstar1.class))),
                  Fstar2.me = median(Fstar, na.rm = TRUE),
                  Fstar2.lo = quantile(Fstar, prob = 0.25, na.rm = TRUE),
                  Fstar2.hi = quantile(Fstar, prob = 0.75, na.rm = TRUE))

p.fstar <- ggplot(dat.fstar, aes(x = Fstar1.num, y = Fstar2.me))
p.fstar <- p.fstar + geom_quantile(data = sge.genes.comp, aes(x = Fstar1, y = Fstar), col = "grey75")
p.fstar <- p.fstar + geom_errorbar(aes(ymin = Fstar2.lo, ymax = Fstar2.hi))
p.fstar <- p.fstar + geom_point()
p.fstar <- p.fstar + xlim(0, 5) + theme_bw() + xlab("F* (ESC)") + ylab("F* (BMDC)")
p.fstar

cor.test(~Fstar+Fstar1, sge.genes.comp, method = "kendall") #***

################################################################################
#
# CENTRALITY MEASURES
#
################################################################################

NetworkCorr <- as.data.frame(matrix(nrow = 7, ncol = 3))
names(NetworkCorr)[1] <- "Measure"
names(NetworkCorr)[2] <- "Correlation with F*"
names(NetworkCorr)[3] <- "p-value"

cor1a <- cor.test(~Fstar+mean.degree.tot, sge.genes, method = "kendall")
NetworkCorr[1, 2] <- cor1a$estimate
NetworkCorr[1, 3] <- cor1a$p.value
NetworkCorr[1, 1] <- "Degree"

cor2a <- cor.test(~Fstar+mean.hub_score, sge.genes, method = "kendall")
NetworkCorr[2, 2] <- cor2a$estimate
NetworkCorr[2, 3] <- cor2a$p.value
NetworkCorr[2, 1] <- "Hub score"

cor3a <- cor.test(~Fstar+mean.authority_score, sge.genes, method = "kendall")
NetworkCorr[3, 2] <- cor3a$estimate
NetworkCorr[3, 3] <- cor3a$p.value
NetworkCorr[3, 1] <- "Authority score"

cor4a <- cor.test(~Fstar+mean.closeness, sge.genes, method = "kendall")
NetworkCorr[4, 2] <- cor4a$estimate
NetworkCorr[4, 3] <- cor4a$p.value
NetworkCorr[4, 1] <- "Closeness"

cor5a <- cor.test(~Fstar+mean.betweenness, sge.genes, method = "kendall")
NetworkCorr[5, 2] <- cor5a$estimate
NetworkCorr[5, 3] <- cor5a$p.value
NetworkCorr[5, 1] <- "Betweenness"

cor6a <- cor.test(~Fstar+mean.transitivity, sge.genes, method = "kendall")
NetworkCorr[6, 2] <- cor6a$estimate
NetworkCorr[6, 3] <- cor6a$p.value
NetworkCorr[6, 1] <- "Transitivity"

# Correlation between Fstar and the number of pathway a gene participates in
cor7a <- cor.test(~Fstar+pleiotropy, sge.genes, method = "kendall")
NetworkCorr[7, 1] <- "Pleiotropy"
NetworkCorr[7, 2] <- cor7a$estimate
NetworkCorr[7, 3] <- cor7a$p.value

# Makes a table with the correlations
write.table(NetworkCorr, "Results/NetworkCorrelations.csv", sep = ",", quote = FALSE, row.names = FALSE)
require(gridExtra)
pdf("Results/NetworkCorrelations.pdf", height = 4, width = 6)
grid.table(NetworkCorr)
dev.off()

# Look at FstarR:
NetworkCorrR <- as.data.frame(matrix(nrow = 7, ncol = 3))
names(NetworkCorrR)[1] <- "Measure"
names(NetworkCorrR)[2] <- "Correlation with F*"
names(NetworkCorrR)[3] <- "p-value"

cor1a <- cor.test(~FstarR+mean.degree.tot, sge.genes, method = "kendall")
NetworkCorrR[1, 2] <- cor1a$estimate
NetworkCorrR[1, 3] <- cor1a$p.value
NetworkCorrR[1, 1] <- "Degree"

cor2a <- cor.test(~FstarR+mean.hub_score, sge.genes, method = "kendall")
NetworkCorrR[2, 2] <- cor2a$estimate
NetworkCorrR[2, 3] <- cor2a$p.value
NetworkCorrR[2, 1] <- "Hub score"

cor3a <- cor.test(~FstarR+mean.authority_score, sge.genes, method = "kendall")
NetworkCorrR[3, 2] <- cor3a$estimate
NetworkCorrR[3, 3] <- cor3a$p.value
NetworkCorrR[3, 1] <- "Authority score"

cor4a <- cor.test(~FstarR+mean.closeness, sge.genes, method = "kendall")
NetworkCorrR[4, 2] <- cor4a$estimate
NetworkCorrR[4, 3] <- cor4a$p.value
NetworkCorrR[4, 1] <- "Closeness"

cor5a <- cor.test(~FstarR+mean.betweenness, sge.genes, method = "kendall")
NetworkCorrR[5, 2] <- cor5a$estimate
NetworkCorrR[5, 3] <- cor5a$p.value
NetworkCorrR[5, 1] <- "Betweenness"

cor6a <- cor.test(~FstarR+mean.transitivity, sge.genes, method = "kendall")
NetworkCorrR[6, 2] <- cor6a$estimate
NetworkCorrR[6, 3] <- cor6a$p.value
NetworkCorrR[6, 1] <- "Transitivity"

# Correlation between Fstar and the number of pathway a gene participates in
cor7a <- cor.test(~FstarR+pleiotropy, sge.genes, method = "kendall")
NetworkCorrR[7, 1] <- "Pleiotropy"
NetworkCorrR[7, 2] <- cor7a$estimate
NetworkCorrR[7, 3] <- cor7a$p.value

# Makes a table with the correlations
write.table(NetworkCorrR, "Results/NetworkCorrelationsR.csv", sep = ",", quote = FALSE, row.names = FALSE)
require(gridExtra)
pdf("Results/NetworkCorrelationsR.pdf", height = 4, width = 6)
grid.table(NetworkCorrR)
dev.off()

################################################################################
#
# CENTRALITY MEASURES : PATHWAY LEVEL
#
################################################################################

NetworkCorr2 <- as.data.frame(matrix(nrow = 3, ncol = 3))
names(NetworkCorr2)[1] <- "Measure"
names(NetworkCorr2)[2] <- "Correlation with F*"
names(NetworkCorr2)[3] <- "p-value"

cor1a <- cor.test(~Mean.Fstar+Size, sge.paths, method = "kendall")
NetworkCorr2[1, 2] <- cor1a$estimate
NetworkCorr2[1, 3] <- cor1a$p.value
NetworkCorr2[1, 1] <- "Size"

cor2a <- cor.test(~Mean.Fstar+Diameter, sge.paths, method = "kendall")
NetworkCorr2[2, 2] <- cor2a$estimate
NetworkCorr2[2, 3] <- cor2a$p.value
NetworkCorr2[2, 1] <- "Diameter"

cor3a <- cor.test(~Mean.Fstar+Transitivity, sge.paths, method = "kendall")
NetworkCorr2[3, 2] <- cor3a$estimate
NetworkCorr2[3, 3] <- cor3a$p.value
NetworkCorr2[3, 1] <- "Transitivity"

# Makes a table with the correlations
write.table(NetworkCorr2, "Results/NetworkCorrelations2.csv", sep = ",", quote = FALSE, row.names = FALSE)
require(gridExtra)
pdf("Results/NetworkCorrelations2.pdf", height = 4, width = 6)
grid.table(NetworkCorr2)
dev.off()

# With FstarR:

NetworkCorr2R <- as.data.frame(matrix(nrow = 3, ncol = 3))
names(NetworkCorr2R)[1] <- "Measure"
names(NetworkCorr2R)[2] <- "Correlation with F*"
names(NetworkCorr2R)[3] <- "p-value"

cor1a <- cor.test(~Mean.FstarR+Size, sge.paths, method = "kendall")
NetworkCorr2R[1, 2] <- cor1a$estimate
NetworkCorr2R[1, 3] <- cor1a$p.value
NetworkCorr2R[1, 1] <- "Size"

cor2a <- cor.test(~Mean.FstarR+Diameter, sge.paths, method = "kendall")
NetworkCorr2R[2, 2] <- cor2a$estimate
NetworkCorr2R[2, 3] <- cor2a$p.value
NetworkCorr2R[2, 1] <- "Diameter"

cor3a <- cor.test(~Mean.FstarR+Transitivity, sge.paths, method = "kendall")
NetworkCorr2R[3, 2] <- cor3a$estimate
NetworkCorr2R[3, 3] <- cor3a$p.value
NetworkCorr2R[3, 1] <- "Transitivity"

# Makes a table with the correlations
write.table(NetworkCorr2R, "Results/NetworkCorrelations2R.csv", sep = ",", quote = FALSE, row.names = FALSE)
require(gridExtra)
pdf("Results/NetworkCorrelations2R.pdf", height = 4, width = 6)
grid.table(NetworkCorr2R)
dev.off()

################################################################################
#
# CENTRALITY MEASURES : PPI
#
################################################################################

cor.test(~Fstar+degree.ppi, sge.genes, method = "kendall")
# negative, **

cor.test(~Fstar+hub_score.ppi, sge.genes, method = "kendall")
# negative, ***

cor.test(~Fstar+closeness.ppi, sge.genes, method = "kendall")
# negative, *** (but closeness value dubious for PPI, see figure below)

cor.test(~Fstar+betweenness.ppi, sge.genes, method = "kendall")
# negative, ***

cor.test(~Fstar+transitivity.ppi, sge.genes, method = "kendall")
# negative, ***

wilcox.test(Fstar~complex.ppi, sge.genes)
sapply(split(sge.genes$Fstar, sge.genes$complex.ppi), mean)
# *** Lower noise for complex interactions

wilcox.test(Fstar~polymer.ppi, sge.genes)
# .


################################################################################
#
# GENE AGE
#
################################################################################

library(ppcor)

# Correcting for sequence divergence
BTage <- sge.genes[c("Ensembl_id", "Fstar", "Age", "KaKsHuman")]
BTage <- na.omit(BTage)
pcor.test(BTage$Fstar, BTage$Age, BTage$KaKsHuman, method = "kendall")
boxplot(sge.genes$Fstar~sge.genes$Age, log = "y")

BTage <- sge.genes[c("Ensembl_id", "FstarR", "Age", "KaKsHuman")]
BTage <- na.omit(BTage)
pcor.test(BTage$FstarR, BTage$Age, BTage$KaKsHuman, method = "kendall")
boxplot(sge.genes$FstarR~sge.genes$Age, log = "y")


################################################################################
#
# SEQUENCE DIVERGENCE
#
################################################################################

cor.test(~Fstar+KaKsHuman, sge.genes, method = "kendall")
cor.test(~FstarR+KaKsHuman, sge.genes, method = "kendall")

# correcting for gene age
pcor.test(BTage$Fstar, BTage$KaKsHuman, BTage$Age, method = "kendall")
plot(y = BTage$Fstar, x = BTage$KaKsHuman)

pcor.test(BTage$FstarR, BTage$KaKsHuman, BTage$Age, method = "kendall")
plot(y = BTage$FstarR, x = BTage$KaKsHuman)

################################################################################
#
# SUPP. FIGURE:
#
################################################################################

library(Hmisc)


get.ggplot <- function(sge.genes, var, label, trans = "none", ...) {
  sge.genes[, "x"] <- sge.genes[, var] #For convenience...
  sge.genes <- subset(sge.genes, !is.na(x))
  sge.genes$Class <- cut2(x = sge.genes[, var], levels.mean = TRUE, ...)
  dat <- ddply(.data = sge.genes, .variables = "Class",
               .fun = plyr::summarize,
               me = median(Fstar, na.rm = TRUE),
               hi = quantile(Fstar, prob = 0.75, na.rm = TRUE),
               lo = quantile(Fstar, prob = 0.25, na.rm = TRUE))
  p <- ggplot(dat, aes(y=me, x=as.numeric(as.character(Class))))
  p <- p + geom_quantile(data = sge.genes, aes(x = x, y = Fstar), col = "grey75")
  p <- p + geom_point() + geom_errorbar(aes(ymin = lo, ymax = hi))
  # Restrict viewport without transforming data:
  r <- range(as.numeric(as.character(dat$Class)))
  p <- p + coord_cartesian(xlim = r)
  p <- p + theme_bw() + ylab("F*") + xlab(label)
  if (trans == "sqrt") p <- p + scale_x_sqrt()
  if (trans == "log") p <- p + scale_x_log10()
  return(p)
}

p.deg <- get.ggplot(sge.genes, "mean.degree.tot", "Pathway degree", "sqrt", g = 15)
p.hub <- get.ggplot(sge.genes, "mean.hub_score", "Pathway hub score", "sqrt", g = 15)
p.aut <- get.ggplot(sge.genes, "mean.authority_score", "Pathway authority score", "sqrt", g = 15)
p.bet <- get.ggplot(sge.genes, "mean.betweenness", "Pathway betweenness", "sqrt", g = 15)
p.clo <- get.ggplot(sge.genes, "mean.closeness", "Pathway closeness", "sqrt", g = 15)
p.tra <- get.ggplot(sge.genes, "mean.transitivity", "Pathway transitivity", "sqrt", g = 15)
p.ple <- get.ggplot(sge.genes, "pleiotropy", "Pathway pleiotropy", "sqrt", g = 15)

p.deg.ppi <- get.ggplot(sge.genes, "degree.ppi", "PPI degree", "sqrt", g = 15)
p.deg.ppi <- p.deg.ppi + coord_cartesian(ylim = c(0.5, 1.5), xlim = c(1.0, 60))
p.hub.ppi <- get.ggplot(sge.genes, "hub_score.ppi", "PPI hub score", "sqrt", g = 15)
p.hub.ppi <- p.hub.ppi + coord_cartesian(ylim = c(0.5, 1.6), xlim = c(0.0, 0.08))
p.bet.ppi <- get.ggplot(sge.genes, "betweenness.ppi", "PPI betweenness", "sqrt", g = 15)
p.bet.ppi <- p.bet.ppi + coord_cartesian(ylim = c(0.5, 1.5), xlim = c(0.0, 4.5e5))

p.clo.ppi <- get.ggplot(sge.genes, "closeness.ppi", "PPI closeness", "sqrt", g = 15) #Closeness values re not informative here, because PPI are all one single big graph
p.tra.ppi <- get.ggplot(sge.genes, "transitivity.ppi", "PPI transitivity", "sqrt", g = 15)

p.kaks <- get.ggplot(sge.genes, "KaKsHuman", "Ka/Ks", "sqrt", g = 15)
p.kaks <- p.kaks + coord_cartesian(ylim = c(0.5, 2), xlim = c(0.005, 0.5))

dat.age <- ddply(.data = sge.genes, .variables = "Age",
                 .fun = plyr::summarize,
                 me = median(Fstar, na.rm = TRUE),
                 hi = quantile(Fstar, prob = 0.75, na.rm = TRUE),
                 lo = quantile(Fstar, prob = 0.25, na.rm = TRUE))
p.age <- ggplot(dat.age, aes(y=me, x=Age))
p.age <- p.age + geom_quantile(data = sge.genes, aes(x = Age, y = Fstar), col = "grey75")
p.age <- p.age + geom_point() + geom_errorbar(aes(ymin = lo, ymax = hi))
p.age <- p.age + theme_bw() + ylab("F*") + xlab("Gene age")

dat.comp <- ddply(.data = sge.genes, .variables = "complex.ppi",
                  .fun = plyr::summarize,
                  me = median(Fstar, na.rm = TRUE),
                  up = quantile(Fstar, prob = 0.75, na.rm = TRUE),
                  lo = quantile(Fstar, prob = 0.25, na.rm = TRUE))
dat.comp <- na.omit(dat.comp)
dat.comp$complex.ppi <- c("Simple", "Complex")
p.comp <- ggplot(dat.comp, aes(y=me, x=complex.ppi))
p.comp <- p.comp + geom_col(fill = grey(0.5)) + geom_point() + geom_errorbar(aes(ymin = lo, ymax = up), width = 0.5)
p.comp <- p.comp + theme_bw() + ylab("F*") + xlab("Complex interactions")
p.comp

dat.poly <- ddply(.data = sge.genes, .variables = "polymer.ppi",
                  .fun = plyr::summarize,
                  me = median(Fstar, na.rm = TRUE),
                  up = quantile(Fstar, prob = 0.75, na.rm = TRUE),
                  lo = quantile(Fstar, prob = 0.25, na.rm = TRUE))
dat.poly <- na.omit(dat.poly)
dat.poly$polymer.ppi <- c("Monomer", "Polymer")
p.poly <- ggplot(dat.poly, aes(y=me, x=polymer.ppi))
p.poly <- p.poly + geom_col(fill = grey(0.5)) + geom_point() + geom_errorbar(aes(ymin = lo, ymax = up), width = 0.5)
p.poly <- p.poly + theme_bw() + ylab("F*") + xlab("Polymer interactions")
p.poly


# We combine plots of significant variables
pg <- plot_grid(p.fstar, ggplot(), ggplot(), p.deg, p.hub, p.aut, p.tra, p.bet, p.clo, p.ple, p.deg.ppi, p.hub.ppi, p.tra.ppi, p.bet.ppi, p.comp, p.poly, p.kaks, p.age, ncol = 3, labels = c("A", "", "", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P"))
pg

ggsave(pg, filename = "Figures/Correlations.pdf", width = 12, height = 16)

