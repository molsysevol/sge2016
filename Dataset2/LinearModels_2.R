# Created on 22/02/2016 by gvbarroso
# Last modified 31/08/2017
# Linear models

################################################################################
#
# GENES ANALYSES
#
################################################################################

sge.genes <- read.table("Results/SGE_GeneData_PCA.txt", stringsAsFactors = FALSE)

m.fstar <- lm(Fstar~PC1 + PC2 + PC3 + PC4 +
                PC5 + PC6 + PC7 + PC8, data = sge.genes)
plot(m.fstar, which = 2)

require(MASS)
bc.fstar <- boxcox(m.fstar, lambda = seq(-0.5, 0.5, len = 500))
l.fstar <- bc.fstar$x[which.max(bc.fstar$y)]
m.fstar.bc <- update(m.fstar, (Fstar^l.fstar -1)/l.fstar~.)
plot(m.fstar.bc, which = 2)
hist(resid(m.fstar.bc))

require(lmtest)
shapiro.test(resid(m.fstar.bc)) #***
hmctest(m.fstar.bc, nsim = 3000) #NS
dwtest(m.fstar.bc) #.
Box.test(resid(m.fstar.bc)[order(predict(m.fstar.bc))], type = "Ljung-Box") #NS

# Trying with FstarR:
m.fstarR <- lm(FstarR~PC1 + PC2 + PC3 + PC4 +
                PC5 + PC6 + PC7 + PC8, data = sge.genes)
plot(m.fstarR, which = 2)

require(MASS)
bc.fstarR <- boxcox(m.fstarR, lambda = seq(-0.5, 0.5, len = 500))
l.fstarR <- bc.fstarR$x[which.max(bc.fstarR$y)]
m.fstarR.bc <- update(m.fstarR, (FstarR^l.fstarR -1)/l.fstarR~.)
plot(m.fstarR.bc, which = 2)
hist(resid(m.fstarR.bc))

require(lmtest)
shapiro.test(resid(m.fstarR.bc)) #***
hmctest(m.fstarR.bc, nsim = 3000) #NS
dwtest(m.fstarR.bc) #NS
Box.test(resid(m.fstarR.bc)[order(predict(m.fstarR.bc))], type = "Ljung-Box") #*


require(rms)
ols.fstar.bc <- ols((Fstar^l.fstar - 1)/l.fstar ~ PC1 + PC2 + PC3 + PC4 +
                      PC5 + PC6 + PC7 + PC8, sge.genes, x = TRUE, y = TRUE)
rob.fstar.bc <- robcov(ols.fstar.bc)
fastbw(rob.fstar.bc) # Keep only 1rst 4 axes
print(rob.fstar.bc)

#Coef    S.E.   t     Pr(>|t|)
#Intercept -0.1148 0.0121 -9.47 <0.0001 
#PC1        0.0558 0.0074  7.49 <0.0001 
#PC2       -0.0384 0.0089 -4.33 <0.0001 
#PC3        0.0492 0.0098  4.99 <0.0001 
#PC4        0.0262 0.0111  2.37 0.0179  
#PC5        0.0253 0.0118  2.14 0.0324  
#PC6       -0.0006 0.0123 -0.05 0.9614  
#PC7       -0.0034 0.0125 -0.27 0.7848  
#PC8       -0.0058 0.0106 -0.54 0.5859  

ols.fstarR.bc <- ols((FstarR^l.fstarR - 1)/l.fstarR ~ PC1 + PC2 + PC3 + PC4 +
                       PC5 + PC6 + PC7 + PC8, sge.genes, x = TRUE, y = TRUE)
rob.fstarR.bc <- robcov(ols.fstarR.bc)
fastbw(rob.fstarR.bc) # Keep only 1rst 4 axes
print(rob.fstarR.bc)

#Coef    S.E.   t     Pr(>|t|)
#Intercept -0.0489 0.0130 -3.75 0.0002  
#PC1        0.0493 0.0074  6.66 <0.0001 
#PC2       -0.0338 0.0083 -4.08 <0.0001 
#PC3        0.0608 0.0105  5.79 <0.0001 
#PC4        0.0365 0.0117  3.12 0.0018  
#PC5        0.0241 0.0117  2.06 0.0396  
#PC6        0.0034 0.0133  0.26 0.7963  
#PC7        0.0060 0.0138  0.44 0.6633  
#PC8       -0.0038 0.0110 -0.35 0.7297  

# Relative importance of each factor:
library(relaimpo)
relimp.fstar <- calc.relimp(m.fstar.bc, rela = TRUE, type = c("lmg", "car"))

relimp.fstar@lmg*100
relimp.fstar@car*100
plot(relimp.fstar@lmg~relimp.fstar@car)

# Model simplification:
fastbw(ols.fstar.bc) # 

# Extra check: quantile regression:
require(quantreg)
q.fstar <- rq(Fstar~PC1 + PC2+ PC3 + PC4 + PC5 + PC6 + PC7 + PC8, data = sge.genes)
summary(q.fstar)

q.fstarR <- rq(FstarR~PC1 + PC2+ PC3 + PC4 + PC5 + PC6 + PC7 + PC8, data = sge.genes)
summary(q.fstarR)


###                   ###
### Correlation model ###
###                   ###

# Note: 3D data are from embryoninc stem cells, so 3D structure might be different for this cell type.

source("../3DGenome/Gls3dClasses.R")

# Have to get rid of NAs:
dat <- sge.genes[,c("Ensembl_id", "Fstar", "PC1", "PC2", "PC3", "PC4", 
                      "PC5", "PC6", "PC7", "PC8")]
dat <- na.omit(dat)
dat$y <- (dat$Fstar^l.fstar - 1)/l.fstar
row.names(dat) <- dat$Ensembl_id

# Get 3D contact information (see 3Dcorrelations.R)
load(file = "Results/3DContactMatrix.Rdata")
dat <- subset(dat, Ensembl_id %in% row.names(contact)) #One gene in not in mm9
cor <- contact[dat$Ensembl_id, dat$Ensembl_id]

# Test of correlation model:
cs <- cor3D(0.12345, inter.data = cor, fixed = FALSE)
cs <- Initialize(cs, data = dat)
corMatrix(cs)


gls.prop.fstar.1 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8 , dat, correlation = cor3D(0.01, cor, fixed = FALSE, prop = TRUE))

gls.bin.fstar.1 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat, correlation = cor3D(0.01, cor, fixed = FALSE, prop = FALSE))

gls.prop.fstar.1$modelStruct$corStruct
# lambda = 0.0165777
gls.bin.fstar.1$modelStruct$corStruct
# lambda = 0.1289445

# Check convergence:
gls.prop.fstar.2 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat, correlation = cor3D(0.8, cor, fixed = FALSE, prop = TRUE))
gls.bin.fstar.2 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat, correlation = cor3D(0.8, cor, fixed = FALSE, prop = FALSE))
gls.prop.fstar.3 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat, correlation = cor3D(0.2, cor, fixed = FALSE, prop = TRUE))
gls.bin.fstar.3 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat, correlation = cor3D(0.2, cor, fixed = FALSE, prop = FALSE))

gls.prop.fstar.2$modelStruct$corStruct
# lambda = 0.007663815
gls.bin.fstar.2$modelStruct$corStruct
# lambda = 0.002453232 
gls.prop.fstar.3$modelStruct$corStruct
# lambda = 0.0004021182
gls.bin.fstar.3$modelStruct$corStruct
# lambda = 0.002453294 

# both models did not converge to the same value. Look at logLik:
logLik(gls.prop.fstar.1)
logLik(gls.prop.fstar.2) #
logLik(gls.prop.fstar.3) #Better!
logLik(gls.bin.fstar.1)
logLik(gls.bin.fstar.2) #Better!
logLik(gls.bin.fstar.3) # Same

# 'null' model:
gls.null.fstar <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat,
                      correlation = cor3D(0., cor, fixed = TRUE))
logLik(gls.null.fstar, REML = FALSE)
# 'log Lik.' -2720.585 (df=10)

gls.nocor.fstar <- gls(y ~ PC1 + PC2 + PC3 + PC4 + PC5 + PC6 + PC7 + PC8, dat)
logLik(gls.nocor.fstar, REML = FALSE)
#'log Lik.' -2720.585 (df=10)

logLik(m.fstar.bc, REML = FALSE)
#'log Lik.' -2721.715 (df=10)
# Not exactly the same because of one missing gene in 3D data, but rather close.

logLik(gls.prop.fstar.3, REML = FALSE)
#'log Lik.' -2720.024 (df=11)
# Better likelihood.
logLik(gls.bin.fstar.2, REML = FALSE)
#'log Lik.' -2720.47 (df=11)
#'
#To compare AIC, we need to use g0 and not m1bc.g, as the latter is fitted by ML and not REML and has more data (some genes have no 3D info and are not included in the GLS analysis)
AIC(gls.prop.fstar.3, gls.bin.fstar.2, gls.null.fstar)
#df      AIC
#gls.prop.fstar.3 11 5527.500
#gls.bin.fstar.2  11 5528.303
#gls.null.fstar   10 5526.679 #Best model, probably because not same cell type.

summary(gls.prop.fstar.3)

#Value   Std.Error   t-value p-value
#(Intercept) -0.11440511 0.012508894 -9.145902  0.0000
#PC1          0.05565200 0.006848248  8.126457  0.0000
#PC2         -0.03855545 0.007627788 -5.054604  0.0000
#PC3          0.04894013 0.009730923  5.029341  0.0000
#PC4          0.02648196 0.011126007  2.380186  0.0174
#PC5          0.02493380 0.011808993  2.111425  0.0348
#PC6         -0.00113841 0.012073287 -0.094291  0.9249
#PC7         -0.00380942 0.012427881 -0.306522  0.7592
#PC8         -0.00609742 0.012602235 -0.483837  0.6285


###
### Look at mean expression instead of F*
###

m.mean <- lm(Mean~PC1 + PC2 + PC3 + PC4 + 
               PC5 + PC6 + PC7 + PC8, data = sge.genes)
require(MASS)
bc.mean <- boxcox(m.mean, lambda = seq(-0.5, 0.5, len = 500))
l.mean <- bc.mean$x[which.max(bc.mean$y)]
m.mean.bc <- update(m.mean, (Mean^l.mean -1)/l.mean~.)
plot(m.mean.bc, which = 2)
hist(resid(m.mean.bc))

shapiro.test(resid(m.mean.bc)) #**
hmctest(m.mean.bc, nsim = 3000) #*

dwtest(m.mean.bc) #NS
Box.test(resid(m.mean.bc)[order(predict(m.mean.bc))], type = "Ljung-Box") #NS

ols.mean.bc <- ols((Mean^l.mean - 1) / l.mean ~ PC1 + PC2 + PC3 + PC4 + 
                  PC5 + PC6 + PC7 + PC8, sge.genes, x = TRUE, y = TRUE)
rob.mean <- robcov(ols.mean)
print(rob.mean)

# Relative importance of each factor:
library(relaimpo)
relimp.mean <- calc.relimp(m.mean.bc, rela = TRUE, type = c("lmg", "car"))

relimp.mean@lmg*100

# Compare the relative importance of factors for Fstar and mean expression:
relimp.fstar.dat <- as.data.frame(relimp.fstar@lmg)
relimp.mean.dat <- as.data.frame(relimp.mean@lmg)
names(relimp.fstar.dat) <- "Relimp"
names(relimp.mean.dat) <- "Relimp"
relimp.fstar.dat$Variable <- "Fstar"
relimp.mean.dat$Variable <- "Mean"
relimp.fstar.dat$Coefficient <- rob.fstar.bc$coefficients[-1]
relimp.mean.dat$Coefficient <- rob.mean$coefficients[-1]
relimp.fstar.dat$Signif <- anova(rob.fstar.bc)[1:8,5]
relimp.mean.dat$Signif <- anova(rob.mean)[1:8,5]
relimp.fstar.dat$Factor <- row.names(relimp.fstar.dat)
relimp.mean.dat$Factor <- row.names(relimp.mean.dat)

relimp <- rbind(relimp.fstar.dat, relimp.mean.dat)

relimp$SignifCode <- as.character(symnum(relimp$Signif, corr = FALSE,
      cutpoints = c(0,  .001,.01,.05, .1, 1),
      symbols = c("***","**","*","."," ")))

relimp$Factor2 <- factor(relimp$Factor, levels = rev(relimp.fstar.dat$Factor),
                         labels = rev(c("PC1", "PC2", "PC3", "PC4", "PC5", "PC6", "PC7", "PC8")))
require(scales)
p <- ggplot(relimp, aes(y = Relimp, x = Factor2))
p <- p + geom_col(aes(fill = Variable), position = "dodge")
p <- p + geom_text(data = subset(relimp, Variable == "Fstar"), aes(y = Relimp, x = Factor2, label = SignifCode), inherit.aes = FALSE, nudge_x = -0.35, nudge_y = 0.02)
p <- p + geom_text(data = subset(relimp, Variable == "Mean"), aes(y = Relimp, x = Factor2, label = SignifCode), inherit.aes = FALSE, nudge_x = 0.15, nudge_y = 0.02)
p <- p + coord_flip() + xlab("Factor") + ylab("Relative importance") + scale_y_continuous(labels = percent)
p <- p + scale_fill_manual(values = c("Fstar" = "black", "Mean" = "grey75"), labels = c("F*", "Mean")) + theme_bw()
p

ggsave(p, filename = "Figures/RelativeImportance.pdf", width = 7, height = 4)

# Now with 3D correlations:
dat2 <- sge.genes[,c("Ensembl_id", "Mean", "PC1", "PC2", "PC3", "PC4", 
                     "PC5", "PC6", "PC7", "PC8")]
dat2 <- na.omit(dat2)
dat2$y <- (dat2$Mean^l.mean-1)/l.mean
row.names(dat2) <- dat2$Ensembl_id
dat2 <- subset(dat2, Ensembl_id %in% row.names(contact)) #One gene in not in mm9

gls.prop.mean.1 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                         PC5 + PC6 + PC7 + PC8,
                       dat2, correlation = cor3D(0.01, cor, fixed = FALSE, prop = TRUE))

gls.bin.mean.1 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                        PC5 + PC6 + PC7 + PC8,
                      dat2, correlation = cor3D(0.01, cor, fixed = FALSE, prop = FALSE))

# Check convergence:
gls.prop.mean.2 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                         PC5 + PC6 + PC7 + PC8,
                       dat2, correlation = cor3D(0.8, cor, fixed = FALSE, prop = TRUE))

gls.bin.mean.2 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                        PC5 + PC6 + PC7 + PC8,
                      dat2, correlation = cor3D(0.8, cor, fixed = FALSE, prop = FALSE))

gls.prop.mean.3 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                         PC5 + PC6 + PC7 + PC8,
                       dat2, correlation = cor3D(0.2, cor, fixed = FALSE, prop = TRUE))

gls.bin.mean.3 <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                        PC5 + PC6 + PC7 + PC8,
                      dat2, correlation = cor3D(0.2, cor, fixed = FALSE, prop = FALSE))

gls.null.mean <- gls(y ~ PC1 + PC2 + PC3 + PC4 + 
                       PC5 + PC6 + PC7 + PC8,
                     dat2, correlation = cor3D(0.0, cor, fixed = TRUE, prop = FALSE))

gls.prop.mean.1$modelStruct$corStruct
#0.00022301
gls.bin.mean.1$modelStruct$corStruct
#0.08120291
gls.prop.mean.2$modelStruct$corStruct
#0.0496425
gls.bin.mean.2$modelStruct$corStruct
#7.495736e-09
gls.prop.mean.3$modelStruct$corStruct
#0.5601981
gls.bin.mean.3$modelStruct$corStruct
#8.390119e-09
# Convergence issues
