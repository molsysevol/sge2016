# Created on 22/02/2016 by gvbarroso
# Last modified 12/09/2017
# Look at GO and pathway enrichments for high and low F*

################################################################################

sge.genes <- read.table("Results/SGE_GeneData.txt", stringsAsFactors = FALSE)

################################################################################
#
# GENE ONTOLOGY ENRICHMENT
#
################################################################################

library(biomaRt)
library(topGO)

# Mart object (used throughout the script)
ensembl <- useMart(host = "www.ensembl.org", "ENSEMBL_MART_ENSEMBL",
                   dataset = "mmusculus_gene_ensembl")

# This gets the gene2GO mapping:
goids <- getBM(attributes = c('ensembl_gene_id','go_id'),
               filters = "ensembl_gene_id",
               values = sge.genes$Ensembl_id,
               mart = ensembl,
               verbose = FALSE)

# List GO terms per gene id:
golist <- split(goids$go_id, goids$ensembl_gene_id)

# Now writes to file:
unlink("Results/Sasagawa_GOmapping.txt")
pb <- txtProgressBar(min = 0, max = length(golist), style = 3)
for (i in 1:length(golist)) {
  setTxtProgressBar(pb, i)
  cat(file = "Results/Sasagawa_GOmapping.txt", names(golist[i]), "\t",
      paste(golist[[i]], collapse = ","), "\n", sep = "", append = TRUE)
}
close(pb)

enrichmentTest <- function(anno, candidates, ontologies = c("MF", "BP", "CC"), desc) {
  results <- NULL
  for (ontology in ontologies) {
    goData <- new("topGOdata",
                  ontology = ontology,
                  nodeSize = 5,
                  allGenes = candidates,
                  description = desc,
                  annotationFun = annFUN.gene2GO,
                  gene2GO = anno)
    resultWeight01Fisher <- runTest(goData, algorithm = "weight01",
                                    statistic = "fisher")
    resultParentChildFisher <- runTest(goData, algorithm = "parentchild",
                                       statistic = "fisher")
    nbNodes <- length(attr(resultWeight01Fisher, "score"))
    tbl <- GenTable(goData,
                    parentChildFisher = resultParentChildFisher,
                    weight01Fisher = resultWeight01Fisher,
                    orderBy = "parentChildFisher",
                    ranksOf = "weight01Fisher",
                    topNodes = nbNodes,
                    numChar = 1000)
    tbl$Ontology <- ontology
    results <- rbind(results, tbl)
  }
  # Correction for multiple testing for all ontologies simultaneously
  results$FDRparentChild <- p.adjust(results$parentChildFisher, method = "fdr")
  results$FDRw01 <- p.adjust(results$weight01Fisher, method = "fdr")
  return(results)
}

format.text <- function(text, lmax) {
  for (i in 1:length(text)) {
    if (nchar(text[i]) > lmax) {
      l <- strsplit(text[i], split = " ")[[1]]
      s <- NA
      t <- 0
      for (j in 1:length(l)) {
        if (is.na(s)) {
          s <- l[j]
        } else {
          s <- paste(s, l[j])
        }
        if (nchar(s) - t > lmax & j < length(l)) {
          t <- nchar(s)
          s <- paste(s, "\n", sep = "")
        }
      }
      text[i] <- s
    }
  }
  return(text)
}

############################## High Noise ######################################

anno <- readMappings(file = "Results/Sasagawa_GOmapping.txt", sep = "\t",
                     IDsep = ",")

# Gets the critical value for Fstar, for which analysis will be made
Fstar <- sge.genes$Fstar
sortFstar <- sort(Fstar, decreasing = FALSE)
HcriticalFstar <- quantile(sortFstar, .90)
HcriticalFstar

candidatesHigh <- as.factor(as.integer(sge.genes$Fstar > HcriticalFstar))
names(candidatesHigh) <- sge.genes$Ensembl_id

resultsHigh <- enrichmentTest(anno, candidatesHigh, desc = "highNoise")

subset(resultsHigh, FDRparentChild < 0.01 & FDRw01 < 0.01)

# no significant category found

################################## Low Noise ###################################

# Gets the critical value for Fstar, for which analyses will be made
sortFstar <- sort(Fstar, decreasing = FALSE)
LcriticalFstar <- quantile(sortFstar, .10)
LcriticalFstar

candidatesLow <- as.factor(as.integer(sge.genes$Fstar < LcriticalFstar))
names(candidatesLow) <- sge.genes$Ensembl_id

resultsLow <- enrichmentTest(anno, candidatesLow, desc = "lowNoise")

subset(resultsLow, FDRparentChild < 0.01 & FDRw01 < 0.01)

# no significant category found

################################################################################
#
# PATHWAY ENRICHMENT ANALYSES
#
################################################################################

library(ReactomePA)

# Entrez ids are necessary for Reactome enrichment
EntrezIDs <- getBM(attributes = c("ensembl_gene_id", "entrezgene"),
                   filters = "ensembl_gene_id",
                   values = sge.genes$Ensembl_id,
                   mart = ensembl,
                   verbose = FALSE)

newGeneIDs <- sge.genes[c("Ensembl_id", "Fstar")]
newGeneIDs <- merge(EntrezIDs, newGeneIDs,
                    by.x = "ensembl_gene_id", by.y = "Ensembl_id", all = TRUE)
dup <- which(duplicated(newGeneIDs$ensembl_gene_id))
newGeneIDs <- newGeneIDs[-(dup),] #deletes genes that map ambiguously from Ensembl to Entrez
newGeneIDs <- na.omit(newGeneIDs) #deletes genes that don't map from Ensembl to Entrez
Entrez <- newGeneIDs$entrezgene

HcriticalFstar <- quantile(newGeneIDs$Fstar, .90)
LcriticalFstar <- quantile(newGeneIDs$Fstar, .10)

highFstarGenes <- subset(newGeneIDs, Fstar > HcriticalFstar)
highFstarNames <- as.character(highFstarGenes$entrezgene)

lowFstarGenes <- subset(newGeneIDs, Fstar < LcriticalFstar)
lowFstarNames <- as.character(lowFstarGenes$entrezgene)

# Highly stochastic genes
zHigh <- enrichPathway(gene = highFstarNames,
                       organism = "mouse",
                       qvalueCutoff = 0.01,
                       minGSSize = 5,
                       pAdjustMethod = "fdr",
                       readable = FALSE)
zHigh
as.data.frame(zHigh)
# 4 pathways, related to chromosome maintenance and replication

write.table(as.data.frame(zHigh), "Results/summaryZhigh.txt", sep = "\t")
zHighTable <- read.table("Results/summaryZhigh.txt", sep = "\t", stringsAsFactors = FALSE)
row.names(zHighTable) <- 1:nrow(zHighTable)

zHighPlot <- dotplot(zHigh, showCategory = 4)
zHighPlot

# Lowly stochastic genes
zLow <- enrichPathway(gene = lowFstarNames,
                      organism = "mouse",
                      qvalueCutoff = 0.01,
                      minGSSize = 5,
                      pAdjustMethod = "fdr",
                      readable = FALSE)
zLow
as.data.frame(zLow)
# Nothing

write.table(as.data.frame(zLow), "Results/summaryZlow.txt", sep = "\t")
zLowTable <- read.table("Results/summaryZlow.txt", sep = "\t", stringsAsFactors = FALSE)
row.names(zLowTable) <- 1:nrow(zLowTable)

zLowPlot <- dotplot(zLow, showCategory = 37)
zLowPlot
# No significant pathway enriched