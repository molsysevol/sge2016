# Created on 22/02/2016 by gvbarroso
# Last modified 31/08/2017
# Linear models

################################################################################
#
# GENES ANALYSES
#
################################################################################

sge.genes <- read.table("Results/SGE_GeneData_PCA.txt", stringsAsFactors = FALSE)

m.fstar <- lm(Fstar~PC1 + PC2 + PC3 + PC4 +
                PC5 + PC6 + PC7, data = sge.genes)
plot(m.fstar, which = 2)

require(MASS)
bc.fstar <- boxcox(m.fstar, lambda = seq(-0.5, 0.5, len = 500))
l.fstar <- bc.fstar$x[which.max(bc.fstar$y)]
m.fstar.bc <- update(m.fstar, (Fstar^l.fstar -1)/l.fstar~.)
plot(m.fstar.bc, which = 2)
hist(resid(m.fstar.bc))

require(lmtest)
shapiro.test(resid(m.fstar.bc)) #*
hmctest(m.fstar.bc, nsim = 3000) #NS
dwtest(m.fstar.bc) #NS
Box.test(resid(m.fstar.bc)[order(predict(m.fstar.bc))], type = "Ljung-Box") #NS

require(rms)
ols.fstar.bc <- ols((Fstar^l.fstar - 1)/l.fstar ~ PC1 + PC2 + PC3 + PC4 +
                      PC5 + PC6 + PC7, sge.genes, x = TRUE, y = TRUE)
rob.fstar.bc <- robcov(ols.fstar.bc)
fastbw(rob.fstar.bc) # Keep nothing!
print(rob.fstar.bc)

# Nothing significant
#Intercept  0.1497 0.0253  5.91 <0.0001 
#PC1       -0.0199 0.0140 -1.42 0.1549  
#PC2       -0.0165 0.0159 -1.04 0.3004  
#PC3        0.0061 0.0203  0.30 0.7638  
#PC4       -0.0356 0.0228 -1.56 0.1187  
#PC5       -0.0084 0.0237 -0.36 0.7217  
#PC6        0.0045 0.0233  0.19 0.8476  
#PC7        0.0386 0.0273  1.41 0.1575  

# Extra check: quantile regression:
require(quantreg)
q.fstar <- rq(Fstar~PC1 + PC2+ PC3 + PC4 + PC5 + PC6 + PC7, data = sge.genes)
summary(q.fstar)
# Nothing significant
